const express = require('express');
const qrCode = require('qrcode');
const cors = require('cors');

const app = express();

app.use(cors());

app.use(express.json({ limit: '10kb' }));
app.use(express.urlencoded({ extended: true, limit: '10kb' }));

app.get('/', (req, res) => {
    res.status(200).end('Hello world!');
});

app.post('/api/v1/qrcode', (req, res) => {

    const userURL = req.body.url;

    //test si userURL est correct

    const qrCodeOptions = {
        errorCorrectionLevel: 'L',
        type: 'image/jpeg',
        quality: 0.3,
        margin: 1,
        color: {
            dark: '#000',
            light: '#FFF'
        }
    };
    
    qrCode.toDataURL(userURL, qrCodeOptions, (err, url) => {
        if (err) throw err;

        res.status(200).json({
            status: 'success',
            data: {
                url: url
            }
        });
    
    });

});

const port = 3000;

app.listen(port, () => {
    console.log(`NodeJs running on port ${port}`);
});